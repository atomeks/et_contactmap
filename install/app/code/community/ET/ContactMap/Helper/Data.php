<?php

class ET_ContactMap_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function getEnabled()
    {
        $configVariable = Mage::getStoreConfig('et_contactmap/global_settings/enabled');
        return $configVariable;
    }

    public function getLongitude()
    {
        $configVariable = Mage::getStoreConfig('et_contactmap/global_settings/longitude');
        return $configVariable;
    }

    public function getLatitude()
    {
        $configVariable = Mage::getStoreConfig('et_contactmap/global_settings/latitude');
        return $configVariable;
    }

    public function getAddress()
    {
        $configVariable = Mage::getStoreConfig('et_contactmap/global_settings/address');
        return $configVariable;
    }
}